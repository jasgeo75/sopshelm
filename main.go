package main

import (
	"fmt"
	"go.mozilla.org/sops/v3/decrypt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

var helmBin = "helm"

func main() {
	args, files := parseArgs(os.Args)

	for _, file := range files {
		if isEncrypted(file) {
			newFile := generateFileName()
			decryptFile(file, newFile)
			defer os.Remove(newFile)
			args = append(args, "--values", newFile)
		} else {
			args = append(args, "--values", file)
		}
	}

	for _, file := range getFilesWithSuffix("templates", ".yaml") {
		if isEncrypted(file) {
			oldFile := generateFileName()
			moveFile(file, oldFile)
			decryptFile(oldFile, file)
			defer moveFile(oldFile, file)
		}
	}

	runHelm(args)
}

func init() {
	helmBinValue := os.Getenv("HELM_BINARY_PATH")
	if helmBinValue != "" {
		helmBin = helmBinValue
	}
}

func parseArgs(args []string) (argsOut []string, files []string) {
	for i := 1; i < len(args); i++ {
		switch args[i] {
		case "-f", "--values":
			i++
			files = append(files, args[i])
		default:
			argsOut = append(argsOut, args[i])
		}
	}
	return
}

func isEncrypted(filename string) bool {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("Can't read file %s. Error: %s", filename, err)
	}

	content := string(bytes)
	indexSops := strings.LastIndex(content, "sops:")
	indexVersion := strings.LastIndex(content, "version:")
	return indexSops != -1 && indexVersion != -1 && indexSops < indexVersion
}

func decryptFile(file, outputFile string) {
	bytes, err := decrypt.File(file, "yaml")
	if err != nil {
		log.Fatalf("There was an error decrypting %s. Check that you provided environment variables for the SOPS to decrypt the files. Error: %s", file, err)
	}

	err = ioutil.WriteFile(outputFile, bytes, 0600)
	if err != nil {
		log.Fatalf("Can't create file %s. Error: %s", file, err)
	}
}

func runHelm(args []string) {
	cmd := exec.Command(helmBin, args...)
	stdoutStderr, err := cmd.CombinedOutput()
	fmt.Printf("%s", stdoutStderr)
	if err != nil {
		log.Fatal(err)
	}
}
