# Example of app of apps pattern
Argocd supports [this pattern](https://argoproj.github.io/argo-cd/operator-manual/cluster-bootstrapping/#app-of-apps-pattern). It can be used to deploy many apps together with one command:

```bash
argocd app create all-of-my-apps --repo "https://gitlab.com/ittennull/sopshelm.git" --path examples/app-of-apps #... rest of arguments

```

File _templates/app1.yaml_ provides additional _secrets.yaml_ file to Helm, it's needed because its location (_examples/app1/secrets.yaml_) is outside _templates_ folder. 

The _templates/app2.yaml_ doesn't have additional parameters because this example has _secret.yaml_ **in** _templates_ folder encrypted.