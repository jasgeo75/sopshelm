# Encrypt secrets.yaml file
This example shows how to define an app in Argocd using Helm.

This app has a standard Helm structure. The only extra thing is _secrets.yaml_ file. This file is encrypted using `sops` and contains key-value pairs where key becomes an environment variable name of a container and the value becomes the value of that environment variable. The value is stored in kubernetes secret.

The example app contains a cronjob that prints all environment variables every minute - you can see that even though the values in _secrets.yaml_ (connection string and password) are encrypted in Git Argocd can decrypt them and continue working as if everything was in plain text.

## Create secrets.yaml
The app1 example already contains the _secrets.yaml_ file but I encrypted it with a key that you have no access to, please erase the file, add secrets and re-encrypt it with your KMS of choice. It boils down to running this command
```bash
sops -e -i secrets.yaml
```
Add additional parameters depending on what you use for encryption. Read [sops documentation](https://github.com/mozilla/sops) to find out.

## Deploy
To deploy the app using Argocd CLI run the command
```bash
argocd app create app1 --repo "https://gitlab.com/ittennull/sopshelm.git" --path examples/app1 --values secrets.yaml #... rest of arguments
```

If you use [app of apps pattern](https://argoproj.github.io/argo-cd/operator-manual/cluster-bootstrapping/#app-of-apps-pattern) check out [app-of-apps example](../app-of-apps) on how to provide additional _secrets.yaml_ file